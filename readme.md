# Docker Symfony v5.0.4

### Requirements

- git
- docker
- docker-compose

### Environments

| Service    | Version |
| ---------- | ------- |
| Apache     | 2.4     |
| PHP        | 7.3     |

### Builds

##### Docker Compose

```yaml
version: '3.7'

services:

  mysql:
    image: mysql:5.7
    command: --default-authentication-plugin=mysql_native_password
    environment:
      MYSQL_ROOT_PASSWORD: password
      MYSQL_USER: www-data
      MYSQL_PASSWORD: www-password
      MYSQL_DATABASE: symfony
    volumes:
      - mysql_data:/var/lib/mysql:rw
    networks:
      - local

  symfony:
    image: teamdunet/symfony:latest
    depends_on:
      - mysql
    links:
      - mysql
    volumes:
      - ./:/var/www/html:rw
    ports:
      - 80:80
      - 443:443
    networks:
      - local

networks:
  local:

volumes:
  mysql_data:
```

Start

```
docker-compose up -d
```

Install

```
docker-compose exec symfony composer install
```

##### Portainer template

```yaml
[
  {
    "type": 1,
    "title": "Symfony",
    "description": "PHP framework Symfony",
    "categories": ["developer"],
    "platform": "linux",
    "logo": "https://symfony.com/logos/symfony_black_03.png",
    "image": "teamdunet/symfony:latest",
    "ports": [
      "80/tcp",
      "443/tcp"
    ]
  }
]
```
